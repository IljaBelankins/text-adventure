from random import randrange, shuffle
from typing import List, Callable
import quiz_minigame
import json
from helper import *
import shop_system
import library
import final_dungeon




def arena_combat(player_in: Player):
    file = open ("arena_enemy_values.json", "r")
    arena_combatants = file.read()
    arena_combatants_converted = json.loads(arena_combatants)
    shuffle(arena_combatants_converted)
    arena_opponent = Opponent (arena_combatants_converted[0]["name"], arena_combatants_converted[0]["health"], arena_combatants_converted[0]["attack"])
    combat_between_player_and_1opponent(player_in, arena_opponent)
    if player_in.combat_prowess == 5:
        print ("\"Oi laddie. I saw you give that last 'un a good thrashin'. You's and I gonna go down into that doongeon fer glory!.\"")
        print ("You have gained Brucebjorn the Dragonslayer as a follower. He will help you in the Final Dungeon.")
        player_in.brucebjorn = True
    if player_in.combat_prowess == 10:
        print ("\"I calculate a high probability of your success in the Final Dungeon. You shall serve as my vanguard.\"")
        print ("You have gained Jeniacti the Psyker as a follower. She will help you in the Final Dungeon.")
        player_in.jeniacti = True

def show_travel_and_activity_options_to_user(player_in: Player, activities_in, location_in): 
        print ("Select location to travel to OR an activity to partake in.")
        for item in location_in:
            print ("Type " + item.title() + " to go to " + item.title() + ".")
        for item in activities_in:
            print ("At the " + player_in.location.title() + " you can " + item + ".")
        combined_options = activities_in 
        combined_options.extend(location_in)
        choice = get_player_choice (combined_options, "Wrong choice.")
        if choice.upper() in location_in:
            player_in.location = choice.upper()
            print ("You are now at " + player_in.location.title() + ".")
        elif choice in activities_in:
            if choice.upper() == "Fight".upper():
                arena_combat(player_in)
            if choice.upper() == "Trade".upper():
                shop_system.shop_system_initaite (player_in)
            if choice.upper() == "Quiz".upper():
                quiz_minigame.puzzle_game_library (player_in)
            if choice.upper() == "Lore".upper():
                library.talk_to_librarian (player_in)
            if choice.upper() == "Start Dungeon".upper():
                final_dungeon.begin_dungeon (player_in)
        return player_in



player_name = input("Hello, brigand. What is your name?\n")

print (player_name + ", huh? I'm the boss here so don't play games with me.")

print ("You useful or just ugly? What weapon you use?")

print ("Select 1 for Sword, 2 for Axe or 3 for Mace.")

is_weapon_correct = False

while is_weapon_correct is False:
    player_weapon_preference = input()

    if player_weapon_preference not in ["1", "2", "3"]:
        print ("That ain't no weapon I ever heard of. Speak properly.")
    else:
        is_weapon_correct = True
        if player_weapon_preference == "1":
            player_weapon_preference = "Sword"
        elif player_weapon_preference == "2":
            player_weapon_preference = "Axe"
        elif player_weapon_preference == "3":
            player_weapon_preference = "Mace"


print ("So you're a " + player_weapon_preference + "-master? Better than nothing.")

player = Player(player_name, player_weapon_preference)

print ("Your name is " + player.name + " and you're a " + player.weapon + "-master. You have " + str(player.health) + " health.")

print ("\"Alright put 'em up.\"" + " The boss attacks you.")

if player_weapon_preference != "Sword":
    player.take_damage(9999)
else: 
    print ("You slash the boss.")
    player.take_damage (1)


enemy = Opponent("Underground Boss", 25, 1)

player = combat_between_player_and_1opponent (player, enemy)

enemy = Opponent ("Boss's Lackey", 10, 1)

print ("\"Boss! No!\" The boss' lackey is distressed at your victory and charges you.")

player = combat_between_player_and_1opponent (player, enemy)

print ("You step outside into the town centre. A kid invites you to play a quiz. You have no freedom and must partake")


quiz_minigame.puzzle_game_library (player)

player.location = "HUB"
travelling = True
while travelling is True:
    file = open ("all_locations_and_travel_options.json", "r")
    extracted_travel_options = file.read()
    converted_travel_options = json.loads(extracted_travel_options)
    available_player_locations = converted_travel_options.get(player.location)
    file = open ("all_locations_and_activity_options.json", "r")
    extracted_activity_options = file.read()
    converted_activity_options = json.loads(extracted_activity_options)
    available_player_activities = converted_activity_options.get(player.location)

    if available_player_locations is None:
        available_player_locations = []

    if available_player_activities is None:
        available_player_activities = []
    
    player = show_travel_and_activity_options_to_user(player, available_player_activities, available_player_locations) 










    









