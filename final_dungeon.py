from helper import *


def combat_between_player_and_final_boss (player_in: Player, opponent_in: Opponent):
    while opponent_in.health > 0 and player_in.health >  0:
        
        enemy_combat_choice = 1
    
        is_combat_choice_correct = False
        while is_combat_choice_correct is False:
            player_combat_choice = input()

            if player_combat_choice not in ["1", "2", "3"]:
                print ("That is not a valid input. Use 1 to attack, 2 to parry and 3 for a feint attack.")
            else:
                is_combat_choice_correct = True
            
        
        if player_combat_choice == "3":
            print (opponent_in.name + " does not fall for your feint and you feel your essence drain.")
            player_in.take_damage_from_final_boss (opponent_in.damage)
        elif player_combat_choice == "1":
            print (opponent_in.name + " moves just enough for your strike to miss and you feel your essence drain.")
            player_in.take_damage_from_final_boss (opponent_in.damage)
        elif player_combat_choice == "2":
            print (opponent_in.name + " does not have a weapon that can be parried. You feel your essence drain.")
            player_in.take_damage_from_final_boss (opponent_in.damage)

        

def begin_dungeon (player_in: Player):
    print ("You begin your descent")
    if player_in.brucebjorn == True:
        print ("Bruncebjorn follows.")
    if player_in.jeniacti == True:
        print ("Jeniacti follows.")
    print ("You are at the first floor. You see the Undead Dragon. The fight begins.")
    enemy = Opponent ("Undead Dragon", 50 , 5)
    combat_between_player_and_1opponent (player_in, enemy)
    if player_in.brucebjorn == False:
        print ("The dragon resurrects and you die.")
        exit
    if player_in.brucebjorn == True:
        print ("The dragon resurrects but Brucebjorn uses his dragon slaying skills to finish it for good.")
        print ("Brucebjorn takes a trophy from the dragon and leaves. What a gloryhound.")
    print ("You proceed to the second floor. There's the mind flayer. Combat starts.")
    enemy = Opponent ("Mind Flayer", 100 , 7)
    combat_between_player_and_1opponent (player_in, enemy)
    if player_in.jeniacti == False:
        print ("The Mind Flayer is bored of you and melts your brain.")
        exit
    if player_in.jeniacti == True:
        print ("Just as the Mind Flayer is about to melt your brain Jeniacti pulls it into a wormhome.")
    print ("You proceed to the final boss.")
    print ("Surprisingly the Unmaker looks like a scrawny man with glasses and arcane clothing.")
    print ("\"Oh great the protagonist object is acting up again. Fine, lemme fix this.\"")
    print ("You do not know what this means but you begin combat with the Unmaker")
    enemy = Opponent ("The Unmaker", 9999, 40)
    combat_between_player_and_final_boss (player_in, enemy)
    
    

