from random import shuffle
import json
from helper import give_player_gold, Player
from typing import List, Callable

def puzzle_game ():
    score = 0
    print ("You begin the quiz.")
    file = open ("question.json", "r")
    #processed_questions_list = file.readlines()
    file_string = file.read()
    processed_questions_list = json.loads(file_string)
    shuffle (processed_questions_list)
    for item in processed_questions_list[:3]:
        print (item["question"])
        answer = input ()
        if answer.upper() == item["answer"].upper():
            score = score + 1
    print (str(score) + "/3")
    return score

def puzzle_game_library (player_in: Player):
    score = 0
    print ("You begin the quiz.")
    file = open ("question.json", "r")
    #processed_questions_list = file.readlines()
    file_string = file.read()
    processed_questions_list = json.loads(file_string)
    shuffle (processed_questions_list)
    for item in processed_questions_list[:3]:
        print (item["question"])
        answer = input ()
        if answer.upper() == item["answer"].upper():
            score = score + 1
    give_player_gold (player_in, 50*score)
    print (str(score) + "/3")
   
    return player_in
