from helper import get_player_choice, Player

def talk_to_librarian (player_in: Player):
    if player_in.true_ending == True:
        print ("Xedilian is dead. There is nothing else of value here.")
    if player_in.true_ending == False:
        print ("You wanna know more about the dungeon? You are " + player_in.name + "? I am Xedilian.")
        print ("To access lore about the dungeon type one of the following: Bosses, First Floor, Second floor, Final Floor or you can Leave.")
        player_choice = get_player_choice (["Bosses", "First Floor", "Second Floor", "Final Floor", "Leave"], "Wrong input.")
        if player_choice == "Bosses":
            print ("The dungeon houses many powerful and awe inspiring beings that guard the dungeon's treasure at the depths.")
            print ("You will die without help on the first two beings. The final one will kill you automatically.")
            print ("But that warning won't stop you though, will it?")
            if "Bosses" not in player_in.read_lore:
                player_in.read_lore.append ("Bosses")
        if player_choice == "First Floor":
            print ("The first floor of the dungeon contains an undead Dragon. You'll need to find a Dragonslayer for it.")
            if "First Floor" not in player_in.read_lore:
                player_in.read_lore.append ("First Floor")
        if player_choice == "Second Floor":
            print ("The second floor of the dungeon contains a Mind Flayer Lord. You'll need a psyker for him.")
            if "Second Floor" not in player_in.read_lore:
                player_in.read_lore.append ("Second Floor")
        if player_choice == "Final Floor":
            print ("The final floor of the dungeon contains the Unmaker. He's so cool, attractive and powerful. You can't beat him. You can only worship him.")
            if "Final Floor" not in player_in.read_lore:
                player_in.read_lore.append ("Final Floor")
        
        if "Bosses" in player_in.read_lore and "First Floor" in player_in.read_lore and "Second Floor" in player_in.read_lore and "Final Floor" in player_in.read_lore:
            print ("I see you are very interested in the dungeon. Tell me why.")
            print ("Type Save Town, Glory or Worship Unmaker.")
            player_choice = get_player_choice (["Save Town", "Glory", "Worship Unmaker"], "Wrong Input.")
            if player_choice == "Save Town" or player_choice == "Glory":
                print ("Ofcourse, that makes sense. Anyway, good luck!")
            if player_choice == "Worship Unmaker":
                print ("Ahh, you are one of us. Then let me tell you the truth. The unmaker is a strange being that created this reality using snakes.")
                print ("We worship it and help it in exchange for eternal life. Join us. We have cookies.")
                print ("You kill Xedilian, loot his personal journals and develop a special chant that will destroy the creature.")
                print ("You are ready to face the final foe.")
                player_in.true_ending = True
    return player_in

            










