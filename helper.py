from typing import List, Callable
from random import randrange, shuffle

class Player():

    def __init__(self, inputed_player_name: str, inputed_weapon: str):
        self.name = inputed_player_name 
        self.combat_prowess = 1
        self.weapon = inputed_weapon
        self.health = 70
        self.max_health = 70
        self.base_damage = 3
        self.location = None
        self.gold = 0
        self.prowess_level = 0
        self.read_lore = []
        self.true_ending = False
        self.lifesteal_item = False
        self.agility_item = False
        self.armour_item = False
        self.purchased_items = []
        self.brucebjorn = False
        self.jeniacti = False

    def take_damage (self, damage_done_on_player: int):
        self.health = self.health - damage_done_on_player
        print ("You have taken " + str(damage_done_on_player) + " damage. Your health is now " + str(self.health))
        if self.health <= 0:
            print ("You are dead.")
            exit ()
    
    def take_damage_from_final_boss (self, damage_done_on_player: int):
        self.health = self.health - damage_done_on_player
        print ("You have taken " + str(damage_done_on_player) + " damage. Your health is now " + str(self.health))
        if self.health <= 0 and self.true_ending == False:
            print ("Your essence drains completely and you return to the void.")
            exit ()
        if self.health <= 0 and self.true_ending == True:
            print ("Using Xedilians notes you bellow a special chant with the last of your strength.")
            print ("\"Raise Exception! \"")
            print ("The Unmaker is shocked. The Unmaker's hold over this reality is slipping.")
            print ("\" NO! Now I will have to rewrite the code all over again! \"")
            print ("The Unmaker disappears into a realm tear which promptly closes. You go back to being a peasant.")
            exit ()





class Opponent():
    def __init__ (self, enemy_name: str, enemy_health: int, enemy_damage:int):
        self.name = enemy_name
        self.health = enemy_health
        self.damage = enemy_damage
    def take_damage (self, damage_done_to_enemy: int):
        self.health = self.health - damage_done_to_enemy
        if self.health <= 0:
            print ("You are victorious.")
        else: 
            print ("You inflict " + str(damage_done_to_enemy) + " damage to " + str(self.name) + ". Opponent health is now " + str(self.health))




def get_player_choice (valid_player_input: List, invalid_choice_statement:str):
    is_player_choice_correct = False
    uppercased_inputs = []
    for item in valid_player_input:
        uppercased_inputs.append(item.upper())
    while is_player_choice_correct is False:
        player_choice = input()

        if player_choice.upper() not in uppercased_inputs:
            print (invalid_choice_statement)
        else:
            is_player_choice_correct = True 
    return player_choice 



def give_player_gold (player_in: Player, gold_given:int):
    player_in.gold = player_in.gold + gold_given
    print ("You receive " + str(gold_given) + " gold. Your gold is now " + str(player_in.gold) + ".")


def upgrade_option_select (player_in: Player):
    print ("You are healed to max health and gain 1 Combat Prowess. You can spend Combat Prowess to improve your attack or total health.")
    print ("Press 1 to improve your attack damage by 2 attack power, press 2 to increase max health by 20 or press 3 to upgrade both by 1 and 10 respectively.")

    player_upgrade_choice = get_player_choice (["1", "2", "3"], "Invalid upgrade input. Use 1, 2 or 3.")
    
    if player_upgrade_choice == "1":
        player_in.base_damage = player_in.base_damage + 2
    elif player_upgrade_choice == "2":
        player_in.max_health = player_in.max_health + 20
    elif player_upgrade_choice == "3":
        player_in.base_damage = player_in.base_damage + 1
        player_in.max_health = player_in.max_health + 10

    player_in.health = player_in.max_health
    return player_in 

def combat_between_player_and_1opponent (player_in: Player, opponent_in: Opponent):
    if player_in.agility_item == True:
        print ("Your Bracers of Agility grant you a first strike.")
        opponent_in.take_damage (player_in.base_damage)

    print ("Entering Battle with "+ opponent_in.name + " . Pick your move: 1 for Attack, 2 for Parry, 3 for Feint Attack.")
    
    while opponent_in.health > 0 and player_in.health >  0:
        player_dealt_damage = False
        enemy_combat_choice = randrange (1,5)

        
            
    
        is_combat_choice_correct = False
        while is_combat_choice_correct is False:
            player_combat_choice = input()

            if player_combat_choice not in ["1", "2", "3"]:
                print ("That is not a valid input. Use 1 to attack, 2 to parry and 3 for a feint attack.")
            else:
                is_combat_choice_correct = True
            
        if enemy_combat_choice == 4:
            if player_in.armour_item == True:
                print (opponent_in.name + " tries to get a lucky COUNTER on you but your THORNSTEEL retaliates.")
                opponent_in.take_damage (player_in.base_damage)
            if player_in.armour_item == False:
                print (opponent_in.name +  " gets lucky and COUNTERS you.")
                player_in.take_damage (opponent_in.damage)
        elif enemy_combat_choice == 1 and player_combat_choice == "3":
            print (opponent_in.name + " advances rapidly and strikes through your feint.")
            player_in.take_damage (opponent_in.damage)
        elif enemy_combat_choice == 2 and player_combat_choice == "1":
            print (opponent_in.name + " parries your attack and ripostes.")
            player_in.take_damage (opponent_in.damage)
        elif enemy_combat_choice == 3 and player_combat_choice == "2":
            print (opponent_in.name + " feints and goes around your guard")
            player_in.take_damage (opponent_in.damage)
        elif enemy_combat_choice == 3 and player_combat_choice == "1":
            print ("You advance rapidly and strike through the " + opponent_in.name + "'s feint.")
            opponent_in.take_damage (player_in.base_damage)
            player_dealt_damage = True
        elif enemy_combat_choice == 1 and player_combat_choice == "2":
            print ("You parry and riposte " + opponent_in.name + ".")
            opponent_in.take_damage (player_in.base_damage)
            player_dealt_damage = True
        elif enemy_combat_choice == 2 and player_combat_choice == "3":
            print ("You feint and go around the " + opponent_in.name + "s guard.")
            opponent_in.take_damage (player_in.base_damage)
            player_dealt_damage = True
        elif enemy_combat_choice == 1 and player_combat_choice == "1":
            print ("You and your opponent clash. Nothing happens.")
        elif enemy_combat_choice == 2 and player_combat_choice == "2":
            print ("You and your opponent parry. With nothing to riposte, nothing happens.")
        elif enemy_combat_choice == 3 and player_combat_choice == "3":
            print ("You and your opponent feint attack. After a brief delay, both of you clash and nothing happens.")

        if player_in.lifesteal_item == True and player_dealt_damage == True:
            player_in.health = player_in.health + player_in.base_damage
            if player_in.health > player_in.max_health:
                player_in.health = player_in.max_health
            print ("You lifesteal from the enemy. You are now at " + str(player_in.health) + ".")
            

        
    player_in = upgrade_option_select (player_in)
    player_in.combat_prowess = player_in.combat_prowess + 1
    return player_in