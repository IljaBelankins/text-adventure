from helper import get_player_choice, Player
import json
from typing import List, Callable

def shop_system_initaite (player_in: Player):
    dynamic_purchase_options = []
    print ("Welcome to the shop.")
    
    file = open ("items_strings.json", "r")
    file_string = file.read()
    processed_file = json.loads(file_string)

    for key, value in processed_file.items():
        if key not in player_in.purchased_items:
            print (value)
            dynamic_purchase_options.append (key)
        
    if len (dynamic_purchase_options) == 0:
        print ("There is nothing left to buy.")
        return player_in
        
    

    print ("Type Exit to exit the Shop.")

    player_choice = get_player_choice(dynamic_purchase_options, "Wrong input.")
    if player_choice == "Lifesteal" and player_in.gold >= 100:
        print ("You now have a Lifesteal Sword. You heal on every attack.")
        player_in.lifesteal_item = True
        player_in.gold -= 100
        player_in.purchased_items.append ("Lifesteal")
        return player_in
    if player_choice == "Lifesteal" and player_in.gold < 100:
        print ("Not enough money.")
        return player_in
    if player_choice == "Agility" and player_in.gold >= 50:
        print ("You now have the Bracers of Agility. You will now strike first.")
        player_in.agility_item = True
        player_in.gold -= 50
        player_in.purchased_items.append ("Agility")
        return player_in
    if player_choice == "Agility" and player_in.gold < 50:
        print ("Not enough money.")
        return player_in
    if player_choice == "Armour" and player_in.gold >= 150:
        print ("You are now wearing Thornsteel. You are now immune to lucky strikes.")
        player_in.armour_item = True
        player_in.gold -= 150
        player_in.purchased_items.append ("Armour")
        return player_in
    if player_choice == "Armour" and player_in.gold < 150:
        print ("Not enough money.")
        return player_in

    return player_in